#include <stdbool.h>
#include <stdint.h>
#include <OctoWS2811.h>
#include "Const.h"
#include "computer.h"

DMAMEM int displayMemory[LEDS_PER_STRIP*6];
int drawingMemory[LEDS_PER_STRIP*6];

const int config = WS2811_GRB | WS2811_800kHz;

OctoWS2811 leds(LEDS_PER_STRIP, displayMemory, drawingMemory, config);

uint32_t lastMillis = 0;

const uint_fast8_t BLINK_PIN = 13;
uint_fast8_t blink = 0;

void setup() {
	// USB to computer
	USB.begin(BAUD_RATE);

	pinMode(BLINK_PIN, OUTPUT);

	// Start dark
	leds.begin();
	leds.show();

	computer_init(&leds);

	// Start default animation
	const char * anim = &DEFAULT_ANIMATION[0];
	while (*anim) {
		computer_input_from_usb(*anim++);
	}
}

void loop() {
	leds.show();

	// From Serial: From the laptop/programmer
	while (USB.available() > 0) {
		// read the incoming byte:
		uint8_t b = Serial.read();
		computer_input_from_usb(b);
	}

	uint32_t now = millis();
	uint8_t elapsed = now - lastMillis;
	lastMillis = now;

	computer_run(elapsed);

	// Blink every 64 updates, to prove we're alive.
	// If the device blinks faster than once per second, our frame rate is good!
	blink++;
	if (blink == 60) {
		blink = 0;
		digitalWrite(BLINK_PIN, HIGH);
	} else {
		digitalWrite(BLINK_PIN, LOW);
	}
}
