#ifndef CONST_H__
#define CONST_H__

#define GRID_WIDTH      (16)
#define GRID_HEIGHT     (12)
#define LEDS_PER_STRIP  (GRID_WIDTH * 3)
#define LED_COUNT       (GRID_WIDTH * GRID_HEIGHT)

#define USB             Serial
#define BAUD_RATE       (57600)

/*****************************************
 * Default: Blinking cursor animation
 *****************************************

x = (X == 0) ? 0.5 : 0;
y = (Y == 0) ? 1 : 0;

led = rgb(sin01(T * 0.6) * min(x, y), 0, 0);

 *
 */

const char * DEFAULT_ANIMATION =
"\x31\x63\x21\x0a\x31\x67\x44\x79\x0a\x31\x73\x21\x3d\x58\x5f\x0a\x31\x73\x22\x3f\x76\x21\x2c\x30\x2e\x35\x0a\x31\x73\x23\x3d\x59\x5f\x0a\x31\x73\x24\x3f\x76\x23\x2c\x31\x0a\x31\x73\x25\x2a\x54\x5f\x2c\x30\x2e\x36\x0a\x31\x73\x26\x73\x76\x25\x0a\x31\x73\x27\x6d\x76\x22\x2c\x76\x24\x0a\x31\x73\x28\x2a\x76\x26\x2c\x76\x27\x0a\x31\x73\x29\x5b\x76\x28\x0a\x31\x63\x2a\x0a"
;

#endif
