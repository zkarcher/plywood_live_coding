#ifndef LED_LAYOUT_H
#define LED_LAYOUT_H

#include <stdbool.h>
#include <stdint.h>
#include "Const.h"

#define TWOPI (6.283185f)

void led_layout_set_all(float * x_ar, float * y_ar)
{
	const float SCALE = 1.0f / GRID_WIDTH;

	// Strips are connected in groups of 3.
	// I'd like to flip the whole board upside-down too.
	for (uint8_t y = 0; y < GRID_HEIGHT; y++) {
		uint8_t iy = (GRID_HEIGHT - 1) - y;

		bool isRtoL = ((iy % 3) == 1);

		for (uint8_t x = 0; x < GRID_WIDTH; x++) {
			uint16_t idx = y * GRID_WIDTH + x;

			uint8_t ix = (GRID_WIDTH - 1) - x;

			x_ar[idx] = (isRtoL ? ((GRID_WIDTH - 1) - ix) : ix) * SCALE;
			y_ar[idx] = iy * SCALE;
		}
	}
}

#endif
